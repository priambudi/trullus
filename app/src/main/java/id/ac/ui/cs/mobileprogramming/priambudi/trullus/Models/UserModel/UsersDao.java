package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.UserModel;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UsersDao {

    @Insert
    void insert(Users users);

    @Query("SELECT * FROM users_table")
    LiveData<List<Users>> getAllUsers();
}
package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "article_table")
public class Favourites {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String title;

    private String content;

    private String datePosted;

    private String authorName;

    private String imageID;

    public Favourites(String title, String content, String imageID,
                      String datePosted, String authorName) {
        this.title = title;
        this.content = content;
        this.imageID = imageID;
        this.datePosted = datePosted;
        this.authorName = authorName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    String getImageID() {
        return imageID;
    }

    String getDatePosted() {
        return datePosted;
    }

    String getAuthorName() {
        return authorName;
    }
}

package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.network;

import java.util.List;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.model.ArticlePostDaring;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GetArticleServiceDaring {
    @GET("/feeds?order=1&user_id=1")
    Call<List<ArticlePostDaring>> getAllArticles();
}
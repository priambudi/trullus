package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Objects;

import android.widget.Toast;

import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.adapter.CustomAdapterDaring;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.model.ArticlePostDaring;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.network.GetArticleServiceDaring;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.network.RetrofitClientInstancePostDaaring;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentArticleOnline extends Fragment {
    public static final String BROADCAST = "id.ac.ui.cs.mobileprogramming.priambudi.trullus.internet";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.feeds, container,false);
        IntentFilter intentFilter = new IntentFilter(BROADCAST);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
        RecyclerView recyclerView = v.findViewById(R.id.recycler_view_feeds);
        recyclerView.setHasFixedSize(true);

        final SwipeRefreshLayout pullToRefresh = v.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
                pullToRefresh.setRefreshing(false);
            }
        });

        ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager
                .NetworkCallback() {
            Intent i = new Intent(BROADCAST);
            Bundle extras = new Bundle();
            @Override
            public void onAvailable(Network network) {
                if (isconnectedToCelular(getContext())){
                    extras.putString("status", "broadcastReceiver");
                    i.putExtras(extras);
                    getContext().sendBroadcast(i);
                } else {
                    extras.putString("status", "available");
                    i.putExtras(extras);
                    getContext().sendBroadcast(i);
                }
            }

            @Override
            public void onLost(Network network) {
                extras.putString("status", "unavailable");
                i.putExtras(extras);
                getContext().sendBroadcast(i);
            }
        };

        ConnectivityManager connectivityManager =
                (ConnectivityManager) Objects.requireNonNull(getContext())
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
        connectivityManager.registerDefaultNetworkCallback(networkCallback);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BROADCAST);
        getContext().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        Objects.requireNonNull(getContext()).unregisterReceiver(broadcastReceiver);
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras.getString("status").equals("broadcastReceiver")) {
                alertCellularData();
                loadData();
            } else if (extras.getString("status").equals("available")) {
                loadData();
            } else {
                alertNoInternet();
            }
        }
    };

    private void loadData() {
        GetArticleServiceDaring service = RetrofitClientInstancePostDaaring.getRetrofitInstance()
                .create(GetArticleServiceDaring.class);

        Call<List<ArticlePostDaring>> call = service.getAllArticles();
        call.enqueue(new Callback<List<ArticlePostDaring>>() {

            @Override
            public void onResponse(@NonNull Call<List<ArticlePostDaring>> call,
                                   @NonNull Response<List<ArticlePostDaring>> response) {
                generateDataList(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<ArticlePostDaring>> call,
                                  @NonNull Throwable t) {
                Toast.makeText(getContext(), getResources().getString(R.string.fetchDataFailed), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void generateDataList(List<ArticlePostDaring> photoList) {
        RecyclerView recyclerView = Objects.requireNonNull(getView())
                .findViewById(R.id.recycler_view_feeds);
        CustomAdapterDaring adapter = new CustomAdapterDaring(getContext(), photoList);
        recyclerView.setAdapter(adapter);
    }

    public boolean isconnectedToCelular(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        }

        Network network = connectivityManager.getActiveNetwork();
        NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(network);
        if (capabilities == null) {
            return false;
        }
        return capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR);
    }

    public void alertCellularData(){
        final AlertDialog.Builder alert = new AlertDialog.Builder(
                Objects.requireNonNull(getContext()));
        alert.setTitle(getResources().getString(R.string.alertCelularTitle));
        alert.setMessage(getResources().getString(R.string.alertCelularSubTitle));
        alert.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        dialog.cancel();
                    }
                });
        alert.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        dialog.cancel();
                    }
                });
        alert.show();
    }

    public void alertNoInternet(){
        final AlertDialog.Builder alert = new AlertDialog.Builder(
                Objects.requireNonNull(getContext()));
        alert.setTitle(getResources().getString(R.string.alertInternetTitle));
        alert.setNegativeButton(getResources().getString(R.string.alertInternetPositive),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        startActivity(new Intent(Settings.ACTION_SETTINGS));
                        dialog.cancel();
                    }
                });
        alert.setPositiveButton(getResources().getString(R.string.alertInternetNegative),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        dialog.cancel();
                    }
                });
        alert.show();
    }

}

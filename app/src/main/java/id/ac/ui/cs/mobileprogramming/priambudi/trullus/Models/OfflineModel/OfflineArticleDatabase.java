package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.OfflineModel;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {OfflineArticle.class}, version = 1, exportSchema = false)
abstract class OfflineArticleDatabase extends RoomDatabase {

    private static OfflineArticleDatabase instance;

    abstract OfflineArticleDao articleDao();

    static synchronized OfflineArticleDatabase getInstance(Context context) {
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    OfflineArticleDatabase.class, "article_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static  RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private OfflineArticleDao offlineArticleDao;

        private PopulateDbAsyncTask(OfflineArticleDatabase db){
            offlineArticleDao = db.articleDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
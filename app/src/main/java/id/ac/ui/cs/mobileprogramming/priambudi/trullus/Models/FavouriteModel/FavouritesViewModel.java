package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class FavouritesViewModel extends AndroidViewModel {
    private FavouritesRepository repository;
    private LiveData<List<Favourites>> allArticles;

    public FavouritesViewModel(@NonNull Application application) {
        super(application);
        repository = new FavouritesRepository(application);
        allArticles = repository.getAllArticles();
    }

    public void insert(Favourites article) {
        repository.insert(article);
    }

    public void update(Favourites article) {
        repository.update(article);
    }

    void delete(Favourites article) {
        repository.delete(article);
    }

    public void deleteAllNotes() {
        repository.deleteAllNotes();
    }

    public LiveData<List<Favourites>> getAllArticles() {
        return allArticles;
    }
}
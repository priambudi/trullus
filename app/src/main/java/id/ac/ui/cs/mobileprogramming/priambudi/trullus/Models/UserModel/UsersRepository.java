package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.UserModel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;


class UsersRepository {
    private UsersDao usersDao;
    private LiveData<List<Users>> allUsers;

    UsersRepository(Application application) {
        UsersDatabase database = UsersDatabase.getInstance(application);
        usersDao = database.usersDao();
        allUsers = usersDao.getAllUsers();
    }

    void insert(Users article) {
        new InsertArticleAsyncTask(usersDao).execute(article);
    }

    LiveData<List<Users>> getAllUsers() {
        return allUsers;
    }

    private static class InsertArticleAsyncTask extends AsyncTask<Users, Void, Void> {
        private UsersDao usersDao;

        private InsertArticleAsyncTask(UsersDao usersDao) {
            this.usersDao = usersDao;
        }

        @Override
        protected Void doInBackground(Users... notes) {
            usersDao.insert(new Users(1, "John Doe", "johndoe@trullus.com", "johndoe", "https://images.unsplash.com/photo-1511314023197-79bcd4a1bfc4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1534&q=80"));
            return null;
        }
    }
}
package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.UserModel;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {Users.class}, version = 1, exportSchema = false)
abstract class UsersDatabase extends RoomDatabase {

    private static UsersDatabase instance;

    abstract UsersDao usersDao();

    static synchronized UsersDatabase getInstance(Context context) {
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    UsersDatabase.class, "users_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static  Callback roomCallback = new Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private UsersDao usersDao;

        private PopulateDbAsyncTask(UsersDatabase db){
            usersDao = db.usersDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            usersDao.insert(new Users(1, "John Doe", "john.doe@trulus.com", "johndoe", "https://images.unsplash.com/photo-1511314023197-79bcd4a1bfc4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1534&q=80"));
            return null;
        }
    }
}
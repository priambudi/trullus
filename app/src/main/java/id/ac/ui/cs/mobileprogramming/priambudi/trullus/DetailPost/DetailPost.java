package id.ac.ui.cs.mobileprogramming.priambudi.trullus.DetailPost;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import java.util.Objects;

import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel.Favourites;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel.FavouritesViewModel;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.OfflineModel.OfflineArticle;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.OfflineModel.OfflineArticleViewModel;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.R;

public class DetailPost extends AppCompatActivity {
    String articleTitle;
    String articleDate;
    String articleAuthor;
    String articleContent;
    String articleImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_post);
        Toolbar myToolbar = findViewById(R.id.detail_toolbar);
        setSupportActionBar(myToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        articleTitle = getIntent().getStringExtra("EXTRA_TITLE");
        articleDate = getIntent().getStringExtra("EXTRA_DATE");
        articleAuthor = getIntent().getStringExtra("EXTRA_AUTHOR");
        articleContent = getIntent().getStringExtra("EXTRA_CONTENT");
        articleImage = getIntent().getStringExtra("EXTRA_IMAGE");
        TextView titleTV = findViewById(R.id.post_title);
        TextView dateTV = findViewById(R.id.date_posted);
        TextView authorTV = findViewById(R.id.author_name);
        TextView contentTV = findViewById(R.id.post_description);
        ImageView imageIV = findViewById(R.id.postimage);

        titleTV.setText(articleTitle);
        dateTV.setText(articleDate);
        authorTV.setText(articleAuthor);
        contentTV.setText(articleContent);

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(this));

        builder.build().load(articleImage)
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(imageIV);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_to_offline:
                OfflineArticleViewModel offlineArticleViewModel = ViewModelProviders.of(this).get(OfflineArticleViewModel.class);
                OfflineArticle note = new OfflineArticle(articleTitle, articleContent, articleImage,articleDate,articleAuthor);

                try{
                    offlineArticleViewModel.insert(note);
                } catch(Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.save_to_favourite:
                FavouritesViewModel favouritesViewModel = ViewModelProviders.of(this).get(FavouritesViewModel.class);
                Favourites fav = new Favourites(articleTitle, articleContent, articleImage,articleDate,articleAuthor);

                try{
                    favouritesViewModel.insert(fav);
                } catch(Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
}

package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.UserModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class UsersViewModel extends AndroidViewModel {
    private UsersRepository repository;
    private LiveData<List<Users>> allArticles;

    public UsersViewModel(@NonNull Application application) {
        super(application);
        repository = new UsersRepository(application);
        allArticles = repository.getAllUsers();
    }

    public LiveData<List<Users>> getAllUsers() {
        return allArticles;
    }
}
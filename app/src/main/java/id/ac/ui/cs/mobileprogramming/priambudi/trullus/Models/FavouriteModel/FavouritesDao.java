package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface FavouritesDao {

    @Insert
    void insert(Favourites article);

    @Update
    void update(Favourites article);

    @Delete
    void delete(Favourites article);

    @Query("DELETE FROM article_table")
    void deleteAllNotes();

    @Query("SELECT * FROM article_table")
    LiveData<List<Favourites>> getAllNotes();
}
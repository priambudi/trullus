package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.UserModel;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.priambudi.trullus.R;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UsersHolder> {
    private UsersViewModel usersViewModel;
    private List<Users> users = new ArrayList<>();

    public UsersAdapter(Context context) {
        usersViewModel = ViewModelProviders.of((FragmentActivity) context).get(UsersViewModel.class);
    }

    @NonNull
    @Override
    public UsersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_profile, parent, false);
        return new UsersHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersHolder holder, final int position) {
        final Users currentUser = users.get(position);
        Picasso.Builder builder = new Picasso.Builder(holder.itemView.getContext());
        builder.downloader(new OkHttp3Downloader(holder.itemView.getContext()));
        builder.build().load(currentUser.getImageID())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.imageViewUser);
        holder.textViewName.setText(currentUser.getName());
        holder.textViewEmail.setText(currentUser.getEmail());
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setUsers(List<Users> notes) {
        this.users = notes;
        notifyDataSetChanged();
    }

    class UsersHolder extends RecyclerView.ViewHolder {
        private TextView textViewName;
        private TextView textViewEmail;
        private ImageView imageViewUser;

        UsersHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.text_view_name);
            textViewEmail = itemView.findViewById(R.id.text_view_email);
            imageViewUser = itemView.findViewById(R.id.image_view_user);
        }
    }
}
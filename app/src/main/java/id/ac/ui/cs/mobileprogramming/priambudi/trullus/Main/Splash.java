package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Main;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.ac.ui.cs.mobileprogramming.priambudi.trullus.R;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        int waktu_loading = 4000;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent home = new Intent(Splash.this, ActivityMain.class);
                startActivity(home);
                finish();

            }
        }, waktu_loading);
    }
}

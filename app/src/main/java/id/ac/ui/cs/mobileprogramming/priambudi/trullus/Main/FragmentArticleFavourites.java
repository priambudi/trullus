package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Objects;

import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel.Favourites;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel.FavouritesAdapter;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel.FavouritesViewModel;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.R;


public class FragmentArticleFavourites extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.feeds, container,false);
        RecyclerView recyclerView = v.findViewById(R.id.recycler_view_feeds);
        recyclerView.setHasFixedSize(true);

        final FavouritesAdapter adapter = new FavouritesAdapter(getContext());
        recyclerView.setAdapter(adapter);

        final SwipeRefreshLayout pullToRefresh = v.findViewById(R.id.pullToRefresh);
        loadData(adapter, pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(adapter, pullToRefresh);
            }
        });
        return v;
    }

    private void loadData(final FavouritesAdapter adapter, SwipeRefreshLayout pullToRefresh) {
        FavouritesViewModel noteViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(FavouritesViewModel.class);
        noteViewModel.getAllArticles().observe(getActivity(), new Observer<List<Favourites>>() {
            @Override
            public void onChanged(@Nullable List<Favourites> articles) {
                adapter.setFavourites(articles);
            }
        });
        pullToRefresh.setRefreshing(false);
    }
}

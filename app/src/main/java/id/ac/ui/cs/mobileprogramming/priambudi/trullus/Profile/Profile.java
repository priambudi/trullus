package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Profile;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;

import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Helpers.CircleTransform;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.R;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.UserModel.Users;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.UserModel.UsersViewModel;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar myToolbar = findViewById(R.id.profile_toolbar);
        setSupportActionBar(myToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final TextView name = findViewById(R.id.text_view_name);
        final TextView email = findViewById(R.id.text_view_email);
        final ImageView profilePicture = findViewById(R.id.image_view_user);

        final UsersViewModel noteViewModel = ViewModelProviders.of(this).get(UsersViewModel.class);
        noteViewModel.getAllUsers().observe(this, new Observer<List<Users>>() {
            @Override
            public void onChanged(@Nullable List<Users> users) {
                name.setText(users != null ? users.get(0).getName() : "null");
                email.setText(users != null ? users.get(0).getEmail() : "null");
                Picasso.Builder builder = new Picasso.Builder(getBaseContext());
                builder.downloader(new OkHttp3Downloader(getBaseContext()));
                builder.build().load(users.get(0).getImageID())
                        .transform(new CircleTransform())
                        .placeholder((R.drawable.ic_launcher_background))
                        .error(R.drawable.ic_launcher_background)
                        .into(profilePicture);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}

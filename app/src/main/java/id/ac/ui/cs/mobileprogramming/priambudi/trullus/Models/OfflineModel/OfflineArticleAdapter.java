package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.OfflineModel;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.DetailPost.DetailPost;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.R;

public class OfflineArticleAdapter extends RecyclerView.Adapter<OfflineArticleAdapter.ArticleHolder> {
    private OfflineArticleViewModel offlineArticleViewModel;
    private List<OfflineArticle> offlineArticles = new ArrayList<>();

    public OfflineArticleAdapter(Context context) {
        offlineArticleViewModel = ViewModelProviders.of((FragmentActivity) context).get(OfflineArticleViewModel.class);
    }

    @NonNull
    @Override
    public ArticleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_item, parent, false);
        return new ArticleHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleHolder holder, final int position) {
        final OfflineArticle currentOfflineArticle = offlineArticles.get(position);
        Picasso.Builder builder = new Picasso.Builder(holder.itemView.getContext());
        builder.downloader(new OkHttp3Downloader(holder.itemView.getContext()));
        builder.build().load(currentOfflineArticle.getImageID())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.imageViewPost);
        holder.textViewTitle.setText(currentOfflineArticle.getTitle());
        holder.textViewDatePosted.setText(currentOfflineArticle.getDatePosted());
        holder.textViewAuthorName.setText(currentOfflineArticle.getAuthorName());
        holder.itemView.setTag(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DetailPost.class);
                intent.putExtra("EXTRA_TITLE", currentOfflineArticle.getTitle());
                intent.putExtra("EXTRA_CONTENT", currentOfflineArticle.getContent());
                intent.putExtra("EXTRA_AUTHOR", currentOfflineArticle.getAuthorName());
                intent.putExtra("EXTRA_DATE", currentOfflineArticle.getDatePosted());
                intent.putExtra("EXTRA_IMAGE", currentOfflineArticle.getImageID());
                view.getContext().startActivity(intent);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new AlertDialog.Builder(v.getContext())
                    .setTitle("Delete article")
                    .setMessage("Are you sure you want to delete this article?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            offlineArticleViewModel.delete(currentOfflineArticle);
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return offlineArticles.size();
    }

    public void setOfflineArticles(List<OfflineArticle> notes) {
        this.offlineArticles = notes;
        notifyDataSetChanged();
    }

    public OfflineArticle getArticlesAt(int position){
        return offlineArticles.get(position);
    }

    class ArticleHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewDatePosted;
        private TextView textViewAuthorName;
        private ImageView imageViewPost;

        ArticleHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDatePosted = itemView.findViewById(R.id.date_posted);
            textViewAuthorName= itemView.findViewById(R.id.author_name);
            imageViewPost = itemView.findViewById(R.id.postimage);
        }
    }
}
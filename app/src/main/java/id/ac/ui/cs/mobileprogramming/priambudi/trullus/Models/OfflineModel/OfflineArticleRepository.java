package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.OfflineModel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

class OfflineArticleRepository {
    private OfflineArticleDao offlineArticleDao;
    private LiveData<List<OfflineArticle>> allNotes;

    OfflineArticleRepository(Application application) {
        OfflineArticleDatabase database = OfflineArticleDatabase.getInstance(application);
        offlineArticleDao = database.articleDao();
        allNotes = offlineArticleDao.getAllNotes();
    }

    void insert(OfflineArticle offlineArticle) {
        new InsertArticleAsyncTask(offlineArticleDao).execute(offlineArticle);
    }

    void update(OfflineArticle offlineArticle) {
        new UpdateArticleAsyncTask(offlineArticleDao).execute(offlineArticle);
    }

    void delete(OfflineArticle offlineArticle) {
        new DeleteArticleAsyncTask(offlineArticleDao).execute(offlineArticle);
    }

    void deleteAllNotes() {
        new DeleteAllArticlesAsyncTask(offlineArticleDao).execute();
    }

    LiveData<List<OfflineArticle>> getAllArticles() {
        return allNotes;
    }

    private static class InsertArticleAsyncTask extends AsyncTask<OfflineArticle, Void, Void> {
        private OfflineArticleDao offlineArticleDao;

        private InsertArticleAsyncTask(OfflineArticleDao offlineArticleDao) {
            this.offlineArticleDao = offlineArticleDao;
        }

        @Override
        protected Void doInBackground(OfflineArticle... notes) {
            offlineArticleDao.insert(notes[0]);
            return null;
        }
    }

    private static class UpdateArticleAsyncTask extends AsyncTask<OfflineArticle, Void, Void> {
        private OfflineArticleDao offlineArticleDao;

        private UpdateArticleAsyncTask(OfflineArticleDao noteDao) {
            this.offlineArticleDao = noteDao;
        }

        @Override
        protected Void doInBackground(OfflineArticle... notes) {
            offlineArticleDao.update(notes[0]);
            return null;
        }
    }

    private static class DeleteArticleAsyncTask extends AsyncTask<OfflineArticle, Void, Void> {
        private OfflineArticleDao offlineArticleDao;

        private DeleteArticleAsyncTask(OfflineArticleDao offlineArticleDao) {
            this.offlineArticleDao = offlineArticleDao;
        }

        @Override
        protected Void doInBackground(OfflineArticle... notes) {
            offlineArticleDao.delete(notes[0]);
            return null;
        }
    }

    private static class DeleteAllArticlesAsyncTask extends AsyncTask<Void, Void, Void> {
        private OfflineArticleDao offlineArticleDao;

        private DeleteAllArticlesAsyncTask(OfflineArticleDao offlineArticleDao) {
            this.offlineArticleDao = offlineArticleDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            offlineArticleDao.deleteAllNotes();
            return null;
        }
    }
}
package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {Favourites.class}, version = 1, exportSchema = false)
abstract class FavouritesDatabase extends RoomDatabase {

    private static FavouritesDatabase instance;

    abstract FavouritesDao articleDao();

    static synchronized FavouritesDatabase getInstance(Context context) {
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    FavouritesDatabase.class, "favourites_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static  Callback roomCallback = new Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private FavouritesDao favouritesDao;

        private PopulateDbAsyncTask(FavouritesDatabase db){
            favouritesDao = db.articleDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
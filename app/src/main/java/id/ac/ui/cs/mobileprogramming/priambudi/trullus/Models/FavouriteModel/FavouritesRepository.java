package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.FavouriteModel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;


class FavouritesRepository {
    private FavouritesDao favouritesDao;
    private LiveData<List<Favourites>> allNotes;

    FavouritesRepository(Application application) {
        FavouritesDatabase database = FavouritesDatabase.getInstance(application);
        favouritesDao = database.articleDao();
        allNotes = favouritesDao.getAllNotes();
    }

    void insert(Favourites article) {
        new InsertArticleAsyncTask(favouritesDao).execute(article);
    }

    void update(Favourites article) {
        new UpdateArticleAsyncTask(favouritesDao).execute(article);
    }

    void delete(Favourites article) {
        new DeleteArticleAsyncTask(favouritesDao).execute(article);
    }

    void deleteAllNotes() {
        new DeleteAllArticlesAsyncTask(favouritesDao).execute();
    }

    LiveData<List<Favourites>> getAllArticles() {
        return allNotes;
    }

    private static class InsertArticleAsyncTask extends AsyncTask<Favourites, Void, Void> {
        private FavouritesDao favouritesDao;

        private InsertArticleAsyncTask(FavouritesDao favouritesDao) {
            this.favouritesDao = favouritesDao;
        }

        @Override
        protected Void doInBackground(Favourites... notes) {
            favouritesDao.insert(notes[0]);
            return null;
        }
    }

    private static class UpdateArticleAsyncTask extends AsyncTask<Favourites, Void, Void> {
        private FavouritesDao favouritesDao;

        private UpdateArticleAsyncTask(FavouritesDao noteDao) {
            this.favouritesDao = noteDao;
        }

        @Override
        protected Void doInBackground(Favourites... notes) {
            favouritesDao.update(notes[0]);
            return null;
        }
    }

    private static class DeleteArticleAsyncTask extends AsyncTask<Favourites, Void, Void> {
        private FavouritesDao favouritesDao;

        private DeleteArticleAsyncTask(FavouritesDao favouritesDao) {
            this.favouritesDao = favouritesDao;
        }

        @Override
        protected Void doInBackground(Favourites... notes) {
            favouritesDao.delete(notes[0]);
            return null;
        }
    }

    private static class DeleteAllArticlesAsyncTask extends AsyncTask<Void, Void, Void> {
        private FavouritesDao favouritesDao;

        private DeleteAllArticlesAsyncTask(FavouritesDao favouritesDao) {
            this.favouritesDao = favouritesDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            favouritesDao.deleteAllNotes();
            return null;
        }
    }
}
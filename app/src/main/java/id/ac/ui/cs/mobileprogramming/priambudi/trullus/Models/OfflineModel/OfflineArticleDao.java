package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.OfflineModel;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface OfflineArticleDao {

    @Insert
    void insert(OfflineArticle offlineArticle);

    @Update
    void update(OfflineArticle offlineArticle);

    @Delete
    void delete(OfflineArticle offlineArticle);

    @Query("DELETE FROM article_table")
    void deleteAllNotes();

    @Query("SELECT * FROM article_table")
    LiveData<List<OfflineArticle>> getAllNotes();
}
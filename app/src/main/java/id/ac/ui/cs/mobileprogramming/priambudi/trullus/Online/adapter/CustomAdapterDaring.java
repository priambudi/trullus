package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import id.ac.ui.cs.mobileprogramming.priambudi.trullus.DetailPost.DetailPost;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.model.ArticlePostDaring;
import id.ac.ui.cs.mobileprogramming.priambudi.trullus.R;

public class CustomAdapterDaring extends RecyclerView.Adapter<CustomAdapterDaring.CustomViewHolder> {

    private List<ArticlePostDaring> dataList;
    private Context context;

    public CustomAdapterDaring(Context context, List<ArticlePostDaring> dataList){
        this.context = context;
        this.dataList = dataList;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView txtTitle;
        TextView txtDate;
        TextView txtAuthor;
        private ImageView coverImage;

        CustomViewHolder(View itemView) {
            super(itemView);

            txtTitle = itemView.findViewById(R.id.text_view_title);
            txtDate = itemView.findViewById(R.id.date_posted);
            txtAuthor = itemView.findViewById(R.id.author_name);
            coverImage = itemView.findViewById(R.id.postimage);
        }
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.article_item, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        final ArticlePostDaring currentArticle = dataList.get(position);
        holder.txtTitle.setText(currentArticle.getJudul());

        String date = currentArticle.getWaktu();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.getDefault());
        Date newDate = null;
        try {
            newDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat formatter = new SimpleDateFormat("d-MM-yyyy", Locale.getDefault());
        final String dateStr = formatter.format(newDate);
        holder.txtDate.setText(dateStr);

        holder.txtAuthor.setText(currentArticle.getAuthorName());


        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));

        builder.build().load(dataList.get(position).getGambarUrl())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.coverImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DetailPost.class);
                intent.putExtra("EXTRA_TITLE", currentArticle.getJudul());
                intent.putExtra("EXTRA_CONTENT", currentArticle.getIsi());
                intent.putExtra("EXTRA_AUTHOR", currentArticle.getAuthorName());
                intent.putExtra("EXTRA_DATE", dateStr);
                intent.putExtra("EXTRA_IMAGE", currentArticle.getGambarUrl());
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

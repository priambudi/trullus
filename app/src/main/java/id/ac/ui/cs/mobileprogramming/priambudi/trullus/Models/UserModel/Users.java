package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.UserModel;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "users_table")
public class Users {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    private String email;

    private String password;

    private String imageID;

    public Users(int id, String name, String email, String password, String imageID) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.imageID = imageID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }
}

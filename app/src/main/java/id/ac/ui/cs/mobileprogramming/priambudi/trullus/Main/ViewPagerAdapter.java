package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private String article;
    private String question;
    private String favourites;

    ViewPagerAdapter(FragmentManager fm, String article, String question, String favourites) {
        super(fm);
        this.article = article;
        this.question = question;
        this.favourites = favourites;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 1) {
            fragment = new FragmentArticleOffline();
        } else if (position == 0) {
            fragment = new FragmentArticleOnline();
        } else if (position == 2) {
            fragment = new FragmentArticleFavourites();
        }
        return fragment;
    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = article;
        } else if (position == 1) {
            title = question;
        } else if (position == 2) {
            title = favourites;
        }
        return title;
    }
}
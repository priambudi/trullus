package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.model;

import com.google.gson.annotations.SerializedName;


public class ArticlePostDaring {

    @SerializedName("id")
    private Integer id;
    @SerializedName("judul")
    private String judul;
    @SerializedName("isi")
    private String isi;
    @SerializedName("gambar_url")
    private String gambarUrl;
    @SerializedName("waktu")
    private String waktu;
    @SerializedName("full_name_author")
    private String authorName;

    public ArticlePostDaring(Integer id, String judul, String isi, String gambarUrl, String waktu, String authorName) {
        this.id = id;
        this.judul = judul;
        this.isi = isi;
        this.gambarUrl = gambarUrl;
        this.waktu = waktu;
        this.authorName = authorName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getGambarUrl() {
        return gambarUrl;
    }

    public void setGambarUrl(String gambarUrl) {
        this.gambarUrl = gambarUrl;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}

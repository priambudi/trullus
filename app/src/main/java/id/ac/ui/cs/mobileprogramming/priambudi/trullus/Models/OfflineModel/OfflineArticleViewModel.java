package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Models.OfflineModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class OfflineArticleViewModel extends AndroidViewModel {
    private OfflineArticleRepository repository;
    private LiveData<List<OfflineArticle>> allArticles;

    public OfflineArticleViewModel(@NonNull Application application) {
        super(application);
        repository = new OfflineArticleRepository(application);
        allArticles = repository.getAllArticles();
    }

    public void insert(OfflineArticle offlineArticle) {
        repository.insert(offlineArticle);
    }

    public void update(OfflineArticle offlineArticle) {
        repository.update(offlineArticle);
    }

    void delete(OfflineArticle offlineArticle) {
        repository.delete(offlineArticle);
    }

    public void deleteAllNotes() {
        repository.deleteAllNotes();
    }

    public LiveData<List<OfflineArticle>> getAllArticles() {
        return allArticles;
    }
}
package id.ac.ui.cs.mobileprogramming.priambudi.trullus.Online.network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PostArticleServiceDaring {
    @FormUrlEncoded
    @POST("/post")
    Call<ResponseBody> postArticle(
            @Field("judul") String title,
            @Field("isi") String content,
            @Field("gambar_url") String imageUri,
            @Field("author_id") String authorId,
            @Field("is_question") String isQuestion
    );
}